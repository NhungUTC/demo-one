interface IParent1{
    v1: number,
}
interface IParent2{
    v2: number,
}
interface Child extends IParent1, IParent2{
    name: string,
}
var t2: Child={
    v1: 22,
    v2: 30,
    name: "Anh Tu"
}
//dung this v1, v2 = undefined
//console.log("test2: v1="+this.v1+" v2= "+this.v2+" name: "+t2.name)
console.log("test2: v1="+t2.v1+" v2= "+t2.v2+" name: "+t2.name)