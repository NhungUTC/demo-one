//-----------overriding
class PriterClass{
    doPrint():void{
        console.log("do print from Parent call...");
    }
}
class StringPrinter extends PriterClass{
    doPrint():void{
        super.doPrint();
        console.log("do print from Child call...");
    }
}

var obj= new StringPrinter();
obj.doPrint();
//class and interface
class ILoand{
    interest: number;
}
class AgriLoan implements ILoand{
    interest: number;
    rebate: number;
    constructor(a: number, b: number){
        this.interest=a;
        this.rebate=b;
    }
}
var agriLoan= new AgriLoan(123, 456);
console.log("interest= ", agriLoan.interest+", rebate= "+agriLoan.rebate);