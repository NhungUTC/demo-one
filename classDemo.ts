class Car{
    //field
    name: string;
    //constructor
    constructor(name: string){
        this.name=name;
    }
    //function
     display():void{
        console.log("Car name: ", this.name);
    }
}
//create object
var car1=new Car("Toyota");
//access field
console.log("call name: ", car1.name);
car1.display()
//class inheritance
class Shape{
    area: number;

    constructor(_area: number){
        this.area=_area;
    }
}
class Circle extends Shape{
    show(): void{
        console.log("Area circle: ",this.area);
    }
}
var C1= new Circle(234);
C1.show();
