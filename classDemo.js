var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Car = /** @class */ (function () {
    //constructor
    function Car(name) {
        this.name = name;
    }
    //function
    Car.prototype.display = function () {
        console.log("Car name: ", this.name);
    };
    return Car;
}());
//create object
var car1 = new Car("Toyota");
//access field
console.log("call name: ", car1.name);
car1.display();
//class inheritance
var Shape = /** @class */ (function () {
    function Shape(_area) {
        this.area = _area;
    }
    return Shape;
}());
var Circle = /** @class */ (function (_super) {
    __extends(Circle, _super);
    function Circle() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Circle.prototype.show = function () {
        console.log("Area circle: ", this.area);
    };
    return Circle;
}(Shape));
var C1 = new Circle(234);
C1.show();
