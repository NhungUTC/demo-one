var customer = {
    firstName: "Nhung",
    lastName: "Bui",
    sayHi: function () {
        return "hi";
    }
};
console.log("customer object");
console.log(customer.firstName + " " + customer.lastName + " " + customer.sayHi());
var shape1 = {
    name: "circle",
    sqare: function () {
        return "dien tich hinh tron";
    }
};
//k goi dc ham
//console.log("shape1: ",shape1.name+" "+shape1.sqare)
var fn = shape1.sqare;
console.log(fn());
//
var shape2 = {
    name: "rectangle",
    sqare: ["dien tich", "chu vi"]
};
console.log("shape2: " + shape2.name + " " + shape2.sqare[0] + " " + shape2.sqare[1]);
var list1 = ["Nhung", "Tu", "Tuan"];
//error
//var list2: nameList=["Nhung", "Tu", "Tuan",3]
console.log(list1[1]);
var t1 = {};
t1.name = "Ha";
t1.subject = "Math";
console.log("name: ", t1.name + " subject: " + t1.subject);
var t2 = {
    v1: 22,
    v2: 30,
    name: "Anh Tu"
};
console.log("test2: v1=" + this.v1 + " v2= " + this.v2 + " name: " + t2.name);
