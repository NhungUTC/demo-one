interface Person{
    firstName: string,
    lastName: string,
    sayHi: ()=> string,
}
var customer: Person={
   firstName: "Nhung",
   lastName: "Bui",
   sayHi: ()=>{
     return "hi";  
   }
}
console.log("customer object")
console.log(customer.firstName+" "+customer.lastName+" "+customer.sayHi())
//-----------------Union type and Interface
interface Shape{
    name: string,
    sqare: string |string[] | (()=>string),
}
var shape1 :Shape={
    name:"circle",
    sqare: ()=>{
        return "dien tich hinh tron"
    }
}
//k goi dc ham
//console.log("shape1: ",shape1.name+" "+shape1.sqare)
var fn: any=shape1.sqare
console.log(fn())
//
var shape2: Shape={
    name:"rectangle",
    sqare:["dien tich", "chu vi"],
}
console.log("shape2: "+shape2.name+" "+shape2.sqare[0]+" "+ shape2.sqare[1])
//-----------array and interface
interface nameList{
    [index: number]:string
}
var list1: nameList=["Nhung", "Tu", "Tuan"]
//error
//var list2: nameList=["Nhung", "Tu", "Tuan",3]
console.log(list1[1])

// interface ages{
//     [index: string]: number,
// }
// var listAge: ages;
// listAge["Nhung"]=23
// listAge["Anh"]= 25
// console.log(listAge["Nhung"]+ " "+listAge["Anh"])
// interface ages { 
//     [index:string]:number 
//  } 
 
//  var agelist:ages; 
//  agelist["John"] = 15 
//------------interface and inhertance
interface Person2{
    name: string,
}
interface Teacher extends Person2{
   subject: string,
}
var t1= <Teacher>{}
t1.name="Ha"
t1.subject="Math"
console.log("name: ", t1.name+" subject: "+t1.subject)
//------------------
