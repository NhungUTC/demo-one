var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
//-----------overriding
var PriterClass = /** @class */ (function () {
    function PriterClass() {
    }
    PriterClass.prototype.doPrint = function () {
        console.log("do print from Parent call...");
    };
    return PriterClass;
}());
var StringPrinter = /** @class */ (function (_super) {
    __extends(StringPrinter, _super);
    function StringPrinter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    StringPrinter.prototype.doPrint = function () {
        _super.prototype.doPrint.call(this);
        console.log("do print from Child call...");
    };
    return StringPrinter;
}(PriterClass));
var obj = new StringPrinter();
obj.doPrint();
//class and interface
var ILoand = /** @class */ (function () {
    function ILoand() {
    }
    return ILoand;
}());
var AgriLoan = /** @class */ (function () {
    function AgriLoan(a, b) {
        this.interest = a;
        this.rebate = b;
    }
    return AgriLoan;
}());
var agriLoan = new AgriLoan(123, 456);
console.log("interest= ", agriLoan.interest + ", rebate= " + agriLoan.rebate);
